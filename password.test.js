/* eslint-disable jest/no-identical-title */
const { checkLength, checkAlphabet, checkDigit, checkSymbol, checkPassword } = require('./password')
describe('Test Password Length', () => {
  test('should 8 characters to be true', () => {
    expect(checkLength('12345678')).toBe(true)
  })
  test('should 7 characters to be false', () => {
    expect(checkLength('1234567')).toBe(false)
  })
  test('should 25 characters to be false', () => {
    expect(checkLength('1111111111111111111111111')).toBe(true)
  })
  test('should 26 characters to be false', () => {
    expect(checkLength('11111111111111111111111111')).toBe(false)
  })
})

describe('Test Alphabetin', () => {
  test('should has alphabet m password', () => {
    expect(checkAlphabet('m')).toBe(true)
  })
  test('should has alphabet A password', () => {
    expect(checkAlphabet('A')).toBe(true)
  })
  test('should has alphabet Z password', () => {
    expect(checkAlphabet('Z')).toBe(true)
  })
  test('should has not alphabet in password', () => {
    expect(checkAlphabet('1111')).toBe(false)
  })
})

describe('Test Digit', () => {
  test('should has 1 in password', () => {
    expect(checkDigit('abc1')).toBe(true)
  })
  test('should has 2 in password', () => {
    expect(checkDigit('abc2')).toBe(true)
  })
})

describe('Test Symbol', () => {
  test('should has symbol in password', () => {
    expect(checkSymbol('11!11')).toBe(true)
  })
  test('should has !% in password', () => {
    expect(checkSymbol('!%')).toBe(true)
  })
})

describe('Test Password', () => {
  test('should password Abcdefgsc. to be false', () => {
    expect(checkPassword('Abcdefgsc.')).toBe(false)
  })
  test('should password Abc to be false', () => {
    expect(checkPassword('Abc')).toBe(false)
  })
  test('should password Abcd1234. to be true', () => {
    expect(checkPassword('Abcd1234.')).toBe(true)
  })
})
